# Global Styles

## Typography

### Base styles

## Color
The use of color in UI design is one of the most important tools for effectively communicating with the user. That is why we strive to always make the use of color in Pseudo intentional. By doing this, the end result is a UI that communicates meaning, conveys visual differentiation, and provides a consistent look and feel.

### Visual Meaning
Certain colors have inherent meaning for a large majority of users, although we recognize that cultural differences are plentiful. For example, we use red to communicate an error.
