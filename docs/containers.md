# Containers

## Address boxes 

These boxes are where the addresses can be found on transactions. 

There are two types of addresses, shipping addresses &amp; billing addresses. Within those subcategories there are 4 main address types:

* __Shipping addresses__
    - Ship to
    - Ship from
* __Billing addresses__ 
    - Bill to
    - Bill from

![Billing address](images/address-sender.png)    ![Receivers address](images/address-receiver.png)
``` css
.address-box {
  background-color: #fcfcfc;
  border: 1px solid #dee3e9;
  padding: 5px 16px;
}
```

![Address container](images/address-container.png)

## Dropdown
Dropdown lists allow the user to select one option or multiple options from a list. Dropdowns are used instead of radio lists and checkbox lists inside of a larger form. They provide more flexibility in the number of options the user can choose from.

![Dropdown List](images/dropdown-example.png)

__General Dropdown Styling__ <br>
Placeholder text: 30% black <br>
Dropdown options text: black 13px; <br>
Line-height: 24px <br>

``` css
.dropdown-dialog {
    background-color: #fcfeff;
    border: solid 1px #5388ff;
    padding-top: 24px;
    padding-left: 24px;
    box-shadow: 1px 2px 4px 0 rgba(0, 0, 0, 0.12);
}

```

## Tabs
![Tabs example](images/tabs-example.png)


Tabs make it easy to explore and switch between different views.

__General Tab Styling__ <br>
Background color: #EDF1F2 <br>
Text color: #9AA1B3 <br>
Text: 16px Roboto Regular All Uppercase
Tab height: 40px <br><br>
_Additional tabs generate to left of first tab_

__Selected Tab Styling__ <br>
Text color: #000000 <br>
Selected tabs are underlined by a 2px height line, using the color that matches that module. 

![Transaction tabs](images/tabs-transaction.png)

``` css
.tab {
  background-color: #EDF1F2;
  height: 40px;
  font-size: 16px;
  font-family: Roboto Regular;
  text-transform: uppercase;
  color: #9AA1B3;
}

.tab-selected {
    color: #000000;
    border-bottom: 2px solid;
    border-color: // module color;
}
```
