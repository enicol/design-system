# Tables 
Data tables display sets of raw data  

![Table Component](images/table-image.png)
<br>
__Supported interactions__  

* Row selection
* Long header titles
* Text editing

## Structure
---
A data table contains a __header row__ at the top that lists column names, followed by rows for data.
<br><br>
Checkboxes should accompany each row if the user needs to select or manipulate data.
<br><br><br>
__Data tables may include:__

- Three or more columns of data
- The ability for users to query and manipulate data

<br>
###  __Table content &amp; Column headers__  
``` css
.table-content {
   font-size: 13px;
   font-family: Roboto Regular;     
   color: #4D4D4E;   
}

.column-heading {
   font-size: 13px;
   font-family: Roboto Regular; // all lowercase    
   color: #9AA1B3; 
}  
```

###  __Text alignment__
- Right-align numeric columns
- Left-aligned text columns   



## Interaction     
---
### Row Selection

When a row is selected, display a background color on the row.

``` css
.selected-row {
    background: transparent;
    border-width: 1px solid;  
    border-color: #7ca2ff;  
} 
```

### Long header titles

Sometimes, column names don’t fit in a container with the recommended 56dp of padding in between columns.<br> There are two options to handle this:

1. Display the full column name and enable horizontal scrolling in the table container.

2. Shorten the column name and display it in full on hover.
<br><br>    
##### Long column names truncated with an ellipse INSERT IMAGE
##### Hovering over a truncated column name INSERT IMAGE

### Inline Text Editing

Tables may require basic text editing (e.g. for editing existing text content, or adding content). Include editable fields within a table and denote them using placeholder text.

``` css
.placeholder-text {
    font-family: Roboto Regular;
    color: #C8CBCC;
}
```
## Specs

< INSERT IMAGE>

1. 36px card header height
2. 22px data row height

![Column padding](images/tables-column-padding.png)

__Padding in between columns__

1. Use a minimum of 24px of padding in between column and column data. 

2. Checkbox icons have a width and height of 18px within a 24px icon container.
